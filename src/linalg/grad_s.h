/* ************************************************************************** */
/* GRAD_S.H                                                                   */
/*                                                                            */
/* Copyright 1999-2000, Fabrice LEBEL                                         */
/* ************************************************************************** */

#ifndef GRAD_S_H
#define GRAD_S_H

#include <stdlib.h>
#include "global.h"
#include "tools.h"

int grad_simple(int, REAL **, REAL [], REAL [], REAL, int *, int);

#endif /* GRAD_S_H */
