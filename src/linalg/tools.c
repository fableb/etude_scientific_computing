/* ************************************************************************** */
/* TOOLS.C                                                                    */
/*                                                                            */
/* Copyright 1999-2000, Fabrice LEBEL                                         */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "tools.h"

/*
 * Alloue de l'espace memoire pour un vecteur de dimension dim
 *
 * Parametres :
 * ============
 *    - lr  :  borne inferieure (0 ou 1)
 *    - dim :  dimension du vecteur
 *
 * Valeurs de retour :
 * ===================
 *    - un pointeur sur l'espace memoire allouee
 *    - un pointeur nul (NULL) si l'allocation de memoire a echouee
 */
REAL *vector(int lr, int dim)
{
   REAL *x = NULL;

   /*
    * Verification des parametres
    */
   if((lr < 0) && (lr > 1)) /* borne inferieure non valide */
      return x;
   if(dim < 2 ) /* dimension < 2 */
      return x;

   /*
    * Allocation de memoire
    */
    if(lr == 1)
      x = malloc((dim+1) * sizeof(REAL)); /* indices 1..dim */
    else
      x = malloc(dim * sizeof(REAL)); /* indices 0..dim-1 */

   return x;
}

/* Libere l'espace memoire alloue pour un vecteur */
void free_vector(REAL x[], int lr, int dim)
{
   /*
    * Verification des parametres
    */
   if(x == NULL)
      return; /* aucun espace memoire n'a ete alloue pour x */

   /*
    * Liberation de la memoire allouee
    */
   free(x);
}

/*
 * Alloue de l'espace memoire pour une matrice de dimension (dim1*dim2)
 *
 * Parametres :
 * ============
 *    - lr  :  borne inferieure (0 ou 1)
 *    - dim :  dimension du vecteur
 *
 * Valeurs de retour :
 * ===================
 *    - un pointeur sur l'espace memoire allouee
 *    - un pointeur nul (NULL) si l'allocation de memoire a echouee
 */
REAL **matrix(int lr, int dim1, int dim2)
{
   REAL **x = NULL;
   int i;

   /*
    * Verification des parametres
    */
   if((lr < 0) && (lr > 1)) /* borne inferieure non valide */
      return x;
   if((dim1 < 2) || (dim2 < 2) ) /* dimension < 2 */
      return x;

   /*
    * Allocation de memoire
    */
    if(lr == 1){
      x = malloc((dim1+1) * sizeof(REAL *)); /* indices 1..dim */
      for(i = 0; i < dim1+1; i++)
         x[i] = malloc((dim2+1) * sizeof(REAL));
    }
    else{
      x = malloc(dim1 * sizeof(REAL *)); /* indices 0..dim-1 */
      for(i = 0; i < dim1; i++)
         x[i] = malloc(dim2 * sizeof(REAL));
    }

   return x;
}

/* Libere l'espace memoire allouee pour une matrice de dimension (dim1*dim2) */
void free_matrix(REAL **x, int lr, int dim1, int dim2)
{
   int i;
   /*
    * Verification des parametres
    */
   if((lr < 0) && (lr > 1)) /* borne inferieure non valide */
      return;
   if((dim1 < 2) || (dim2 < 2) ) /* dimension < 2 */
      return;
   if(x == NULL) /* aucun espace memoire n'a ete alloue pour x */
      return;

   /*
    * Liberation de la memoire allouee
    */
   for(i = 0; i < dim1; i++)
      free(x[i]);

   free(x);
}

/* Renvoit le produit scalaire <v,w> */
REAL scalarproduct(REAL v[], REAL w[], int dim, int *err_code)
{
   REAL produit_scalaire = ZERO;
   int i;

/*   for( ; dim-- != 0; ){*/
   for(i = 0; i< dim; i++){
/*      produit_scalaire += (*v++) * (*w++);*/
      produit_scalaire += v[i] * w[i];
      if(ABS(produit_scalaire) > REAL_MAX){ /* depassement de capacite */
         *err_code = ERR_OVERFLOW;
         return 0.0;
      }
      if(ABS(produit_scalaire) < REAL_MIN){ /* sous-depassement de capacite */
         *err_code = ERR_UNDERFLOW;
         return 0.0;
      }
   }

   return produit_scalaire;
}

/* Calcule le produit matriciel w = A * v, avec A(dim,dim), v(dim), w(dim) */
int matvectproduct(REAL **A, REAL v[], REAL w[], int dim)
{
   int i, j;
   REAL temp;

   for(i = 0; i < dim; i++){ /* parcours des lignes de A */
      temp = ZERO;
      for(j = 0; j < dim; j++) /* parcours des colonnes de A */
         temp += A[i][j] * v[j];

      w[i] = temp;
   }
   return 0;
}

/* Affiche un vecteur sur la sortie standard */
void printvect(REAL v[], int dim)
{
   int i;

   for( i = 0; i < dim; i++){
      printf(DELIMITOR);
      printf(R_FORMAT126, *v++);
      printf(DELIMITOR);
      printf("\n");
   }
   printf("\n");
}

/* Affiche une matrice de dimension (dim1*dim2) sur la sortie standard */
void printmat(REAL **A, int dim1, int dim2)
{
   int i, j;

   for(i = 0; i < dim1; i++){
      printf(DELIMITOR);
      for(j = 0; j < dim2; j++){
         if(j > 5)
            break;
         printf(R_FORMAT126, A[i][j]);
      }
      printf(DELIMITOR);
      printf("\n");
   }
   printf("\n");
}
