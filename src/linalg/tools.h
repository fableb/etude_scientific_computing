/* ************************************************************************** */
/* TOOLS.H                                                                    */
/*                                                                            */
/* Copyright 1999-2000, Fabrice LEBEL                                         */
/* ************************************************************************** */

#ifndef TOOLS_H
#define TOOLS_H

#include "global.h"

/* Routines d'allocation memoire */
REAL *vector(int, int);
void free_vector(REAL [], int, int);
REAL **matrix(int, int, int);
void free_matrix(REAL **, int, int, int);
/* Routines de calcul */
REAL scalarproduct(REAL [], REAL [], int , int *);
int matvectproduct(REAL **, REAL [], REAL [], int);
/* Routines d'affichage */
void printvect(REAL [], int);
void printmat(REAL **, int, int);

#endif /* TOOLS_H */
