/* ************************************************************************** */
/* GLOBAL.H                                                                   */
/*                                                                            */
/* Copyright 1999-2000, Fabrice LEBEL                                         */
/* ************************************************************************** */

#ifndef GLOBAL_H
#define GLOBAL_H

#include <float.h>

/*******************************************************************************
                                 MACROS
*******************************************************************************/
#ifdef FLOAT
#define FLOAT_USED
typedef float  REAL;
typedef double LONG_REAL;

/* epsilon machine */
#ifdef FLT_EPSILON /* compilateur ANSI */
#define REAL_EPSILON (REAL) FLT_EPSILON /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_EPSILON real_epsilon()
#endif

/* Plus petit nombre reel positif de la norme IEEE */
#ifdef FLT_MIN /* compilateur ANSI */
#define REAL_MIN (REAL) FLT_MIN /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MIN real_min()
#endif

/* Plus grand nombre reel positif de la norme IEEE */
#ifdef FLT_MAX /* compilateur ANSI */
#define REAL_MAX (REAL) FLT_MAX /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MAX real_max()
#endif

/* Plus petit exposant binaire */
#ifdef FLT_MIN_EXP /* compilateur ANSI */
#define REAL_MIN_EXP FLT_MIN_EXP /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MIN_EXP -125
#endif

/* Plus grand exposant binaire */
#ifdef FLT_MAX_EXP /* compilateur ANSI */
#define REAL_MAX_EXP FLT_MAX_EXP /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MAX_EXP +128
#endif

#define R_FORMAT  "%f"
#define LR_FORMAT "%lf"

#else
#ifdef LDOUBLE
#define LDOUBLE_USED
typedef long double REAL;
typedef long double LONG_REAL;

/* epsilon machine */
#ifdef LDBL_EPSILON /* compilateur ANSI */
#define REAL_EPSILON (REAL) LDBL_EPSILON /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_EPSILON real_ldbl_epsilon()
#endif

/* Plus petit nombre reel positif de la norme IEEE */
#ifdef LDBL_MIN /* compilateur ANSI */
#define REAL_MIN (REAL) LDBL_MIN /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MIN real_ldbl_min()
#endif

/* Plus grand nombre reel positif de la norme IEEE */
#ifdef LDBL_MAX /* compilateur ANSI */
#define REAL_MAX (REAL) LDBL_MAX /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MAX real_ldbl_max()
#endif

/* Plus petit exposant binaire */
#ifdef LDBL_MIN_EXP /* compilateur ANSI */
#define REAL_MIN_EXP LDBL_MIN_EXP /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MIN_EXP -16381
#endif

/* Plus grand exposant binaire */
#ifdef LDBL_MAX_EXP /* compilateur ANSI */
#define REAL_MAX_EXP LDBL_MAX_EXP /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MAX_EXP +16384
#endif

#define R_FORMAT  "%lf"
#define LR_FORMAT "%lf"

#else /* Type par defaut */
#define DEFAULT_USED
typedef double REAL;
typedef long double LONG_REAL;

/* epsilon machine */
#ifdef DBL_EPSILON /* compilateur ANSI */
#define REAL_EPSILON (REAL) DBL_EPSILON /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_EPSILON real_dbl_epsilon()
#endif

/* Plus petit nombre reel positif de la norme IEEE */
#ifdef DBL_MIN /* compilateur ANSI */
#define REAL_MIN (REAL) DBL_MIN /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MIN real_dbl_min()
#endif

/* Plus grand nombre reel positif de la norme IEEE */
#ifdef DBL_MAX /* compilateur ANSI */
#define REAL_MAX (REAL) DBL_MAX /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MAX real_dbl_max()
#endif

/* Plus petit exposant binaire */
#ifdef DBL_MIN_EXP /* compilateur ANSI */
#define REAL_MIN_EXP DBL_MIN_EXP /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MIN_EXP -1021
#endif

/* Plus grand exposant binaire */
#ifdef DBL_MAX_EXP /* compilateur ANSI */
#define REAL_MAX_EXP DBL_MAX_EXP /* cf. float.h */
#else /* compilateur non ANSI */
#define REAL_MAX_EXP +1024
#endif

#define R_FORMAT     "%lf"
#define R_FORMAT126  "%12.6lf"
#define LR_FORMAT    "%lf"

#endif /* fin LDOUBLE */
#endif /* fin FLOAT */

/* Evite les problemes de conversion lies a certains compilateurs */
#define ZERO      (REAL)0.0
#define ONE       (REAL)1.0
#define TWO       (REAL)2.0
#define THREE     (REAL)3.0
#define FOUR      (REAL)4.0
#define FIVE      (REAL)5.0
#define SIX       (REAL)6.0
#define SEVEN     (REAL)7.0
#define EIGHT     (REAL)8.0
#define NINE      (REAL)9.0
#define TEN       (REAL)10.0
#define HALF      (REAL)0.5

#undef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b)) /* Max. entre a et b */
#undef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b)) /* Min. entre a et b */
#undef ABS
#define ABS(a) (((a) < ZERO) ? -(a) : (a)) /* valeur absolue de a */
#undef SQR
static REAL sqrarg;
#define SQR(a) (((sqrarg = (a)) == ZERO) ? ZERO : sqrarg*sqrarg)/* carree de a */
#define DELIMITOR "!"
#define SPACE1    " "

/* Erreurs communes aux fonctions */
enum{
   ERR_MEMORY_ALLOCATION = 1, /* erreur d'allocation d'espace memoire */
   ERR_DIM = 2,               /* dimensions incorrectes */
   ERR_DIV_BY_ZERO = 100,     /* division par zero */
   ERR_UNDERFLOW = 101,       /* sous-depassement de capacite sur les reels */
   ERR_OVERFLOW = 102         /* depassement de capacites sur les reels */
};

enum{
   FALSE = 0,
   TRUE = 1
};
#endif /* GLOBAL_H */
