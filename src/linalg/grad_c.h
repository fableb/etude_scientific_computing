/* ************************************************************************** */
/* GRAD_S.H                                                                   */
/*                                                                            */
/* Copyright 1999-2000, Fabrice LEBEL                                         */
/* ************************************************************************** */

#ifndef GRAD_C_H
#define GRAD_C_H

#include <stdio.h>
#include "global.h"
#include "tools.h"

int grad_conjugue(int, REAL **, REAL [], REAL [], REAL, int *, int);

#endif /* GRAD_C_H */
