/* ************************************************************************** */
/* GRAD_S.C                                                                   */
/*                                                                            */
/* Copyright 1999-2000, Fabrice LEBEL                                         */
/* ************************************************************************** */

#include "grad_s.h"

/*
 * grad_simple: Resoud le systeme A*x = b par la methode du gradient simple.
 *
 * Parametres d'entree:
 * ====================
 *    - dim :  dimension du systeme
 *    - A   :  matrice carre symetrique definie positive du systeme
 *    - x   :  vecteur initial (initialise a zero ou proche de la solution)
 *    - b   :  vecteur second membre du systeme
 *    - eps : precision demandee
 *    - maxiter : nombre maximal d'iterations
 *
 * Parametres de sortie:
 * =====================
 *    - x      :  solution du systeme
 *    - nbiter :  nombre d'iterations effectuees
 *
 * Valeurs de retour:
 * ==================
 *    - 0 : solution trouvee ou nombre maximal d'iterations atteint
 *    - ERR_DIM : dimension du systeme < 2
 *    - ERR_MEMORY_ALLOCATION : memoire insuffisante
 */
int grad_simple(int dim, REAL **A, REAL x[], REAL b[], REAL eps, int *nbiter, int maxiter)
{
   REAL *r;          /* vecteur auxiliaire des residus: r(k) = Ax(k) - b */
   REAL *Ar;         /* vecteur auxiliaire A * r */
   REAL *Ax;         /* vecteur auxiliaire A * x */
   REAL alpha;       /* pas optimal de la methode */
   REAL nume, denom; /* variables auxiliaires pour le calcul de alpha */
   REAL normb2;       /* norme du vecteur b au carre; evite le recalcul */
   int i, j;
   int err_code;

   if(dim < 2)
      return ERR_DIM;

   /*
    * Allocation de memoire
    */
   /* A FAIRE : creer un gestionnaire de memoire... */
   if((r = vector(0, dim)) == NULL)
      return ERR_MEMORY_ALLOCATION;
   if((Ar = vector(0, dim)) == NULL){
      free_vector(r, 0, dim);
      return ERR_MEMORY_ALLOCATION;
   }
   if((Ax = vector(0, dim)) == NULL){
      free_vector(r, 0, dim);
      free_vector(Ar, 0, dim);
      return ERR_MEMORY_ALLOCATION;
   }

   *nbiter = 0;
   normb2 = scalarproduct(b, b, dim, &err_code);
   for(j = 1; j <= maxiter; j++)
   {
      (*nbiter)++;

      /*
       * Calcul du residu
       */
      matvectproduct(A, x, Ax, dim);
      for(i = 0; i < dim; i++)
         r[i] = Ax[i] - b[i];

      /*
       * Calcul du pas optimal
       */
      matvectproduct(A, r, Ar, dim); /* calcul de Ar = A*r */
      nume = scalarproduct(r, r, dim, &err_code);
      denom = scalarproduct(Ar, r, dim, &err_code);

      /* Test de division par zero <=> residus nuls => solution atteinte ! */
      if(ABS(denom - ZERO) < REAL_EPSILON){
         /* liberation de la memoire allouee */
         free_vector(r, 0, dim);
         free_vector(Ar, 0, dim);
         free_vector(Ax, 0, dim);
         return 0;
      }

      alpha = nume / denom;

      /*
       * Calcul de l'iteration
       */
      for(i = 0; i < dim; i++) /* calcul de x(k+1) = x(k) - alpha(k)*r(k) */
         x[i] = x[i] - (alpha * r[i]);
      for(i = 0; i < dim; i++) /* calcul de r(k+1) = r(k) - alpha(k)*Ar(k) */
         r[i] = r[i] - (alpha * Ar[i]);

      /*
       * Test d'arret
       */
      if(scalarproduct(r, r, dim, &err_code) < (SQR(eps) * normb2)){
         /* liberation de la memoire allouee */
         free_vector(r, 0, dim);
         free_vector(Ar, 0, dim);
         free_vector(Ax, 0, dim);
         return 0;
      }
   }

   /* liberation de la memoire allouee */
   free_vector(r, 0, dim);
   free_vector(Ar, 0, dim);
   free_vector(Ax, 0, dim);

   return 0;
}
