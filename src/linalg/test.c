/* TEST.C */

#include <stdio.h>
#include <ctype.h>
#include "global.h"

FILE *f;
char carac;

int next_char();
int is_printable_char(char);

int main()
{
   return 0;
}

/*
 * Parametres d'entree:
 * ====================
 *    - f   : le fichier lu
 *
 * Parametres de sortie:
 * =====================
 *    - a   : la matrice ou l'on stocke les donnees
 *    - n   : la taille de la matrice
 */
int load_fortran_file(FILE *f, REAL **a, int n)
{
   int signe = 1;
   int dim_trouvee = FALSE; /* indique si la dimension est trouvee */

   do{
      carac = next_char();

      if(carac == '#')	/* commentaire Fortran */
      {
         /* un commentaire se poursuit jusqu'a la fin de la ligne */
         do{
            carac = next_char();
         }
         while(carac != '\n');/* on s'arrete au caractere nouvelle ligne */
      }
      /* un reel commence par . ou - ou + ou un chiffre compris entre 0..9 */
      else if((carac == '.') || (carac == '-') || (carac == '+') ||
               isdigit(carac))
      {
         REAL valeur = 0.0;
         
         if(carac == '-'){ /* sauvegarde du signe du nombre */
            signe = -1;
            carac = next_char();
         }
         else if(carac == '+'){
            signe = 1;
            carac = next_char();
         }

         while(isdigit(carac)){ /* Traitement de la partie entiere */
            valeur = (valeur * 10.0) + (carac - '0');
            carac = next_char();
            dim_trouvee = TRUE;
         }

         if(carac == '.'){ /* Traitement des chiffres decimaux */
            int nb_chiffres_decimaux = 0, i;

            while(isdigit(carac)){
               valeur = (valeur * 10.0) + (carac - '0');
               nb_chiffres_decimaux++;
               carac = next_char();
            }
            for(i=0; i <= nb_chiffres_decimaux; i++){
               valeur = valeur / 10.0;
            }
         }
         valeur *= signe; 
      }
   }
   while(carac != EOF);

   return 0;
}

/* Retourne tous les caracteres visibles */
int next_char()
{
   char c;

   c = fgetc(f);
   if(c == EOF)
      return(c);
   else if(c == '\n'){
      return(c);
   }
   else{
      if(!is_printable_char(c))  /* On ne traite pas les caracteres invisibles */
         c = next_char();        /* Appel recursif */
      return (c);
   }
}

/* Determine si un caractere est imprimable */
int is_printable_char(char ch)
{
	switch(ch)
   {
      /* Ensemble des caracteres invisibles: Codes ascii #0 � #31, #127 */
   	case 0x0: /* #0 ... */
      case 0x1:
      case 0x2:
      case 0x3:
      case 0x4:
      case 0x5:
      case 0x6:
      case 0x7: /* beep */
      case 0x8: /* backspace */
      case 0x9: /* tabulation horizontale */
      case 0xa: /* saut de ligne */
      case 0xb: /* tabulation verticale */
      case 0xc: /* saut de page */
		case 0xd: /* retour chariot */
      case 0xe:
      case 0xf:
      case 0x10:
      case 0x11:
      case 0x12:
      case 0x13:
      case 0x14:
      case 0x15:
      case 0x16:
      case 0x17:
		case 0x18:
      case 0x19:
      case 0x1a:
      case 0x1b:
      case 0x1c:
      case 0x1d:
      case 0x1e:
      case 0x1f: /* #31 */
      case 0x7f: /* #127 */
      	return (FALSE);
		/* Tous les autres caracteres ascii sont imprimables */
      default:
      	return (TRUE);
   }
}
