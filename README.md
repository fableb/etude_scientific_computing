This project groups my codes in **Scientific Computing** subjects learned so far.

Author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com

# NVidia CUDA and Code::Blocks

This section describes how to setup CUDA compiler/linker in Code::Blocks.

Original article: https://forums.developer.nvidia.com/t/how-to-setup-cuda-compiler-on-code-blocks-author-not-me/11760


![Alt text](./media/cb_cuda1.png?raw=true "Toolchain executables")

![Alt text](./media/cb_cuda2.png?raw=true "Search directories")

## Debug symbols

To add debug information, you must add the following flags to your compiler options:

```-g -G```

![Alt text](./media/cb_cuda3.png?raw=true "Traditional GCC compliler option")

![Alt text](./media/cb_cuda4.png?raw=true "NVCC compiler option")
